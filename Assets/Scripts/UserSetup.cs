﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserSetup : MonoBehaviour {
    public Animator Cursor;
    public Animator psi;
    public Animator window;
    public AnimationClip introAnim;
    public AudioSource source;

    private bool isHighlighted = false;
    public bool inSubMenu = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 headPosition = Camera.main.transform.position;
        Vector3 gazeDirection = Camera.main.transform.forward;

        RaycastHit hitInfo;



        if (Input.GetMouseButtonDown(0) && !psi.GetCurrentAnimatorStateInfo(0).IsName("ElementsIntro")) 
        {
            
            if (Physics.Raycast(headPosition, gazeDirection, out hitInfo, 75.0f, Physics.DefaultRaycastLayers))
            {

                if (hitInfo.collider.gameObject.tag == "Interactable")
                {
                    hitInfo.collider.gameObject.GetComponent<Button>().onClick.Invoke();

                }

            }
        }

        if (!isHighlighted)
        {
            if (Physics.Raycast(headPosition, gazeDirection, out hitInfo, 75.0f, Physics.DefaultRaycastLayers))
            {

                if (hitInfo.collider.gameObject.tag == "Interactable")
                {
                    Cursor.SetInteger("State", 1);
                    isHighlighted = true;

                }

            }
        }

        if (isHighlighted)
        {
            if (!Physics.Raycast(headPosition, gazeDirection, out hitInfo, 75.0f, Physics.DefaultRaycastLayers))
            {
                
                Cursor.SetInteger("State", 0);
                isHighlighted = false;
            }
        }
	}

    public void Purchase()
    {
        Application.OpenURL("http://www.psitireinflation.com/parts/thru-t");
    }

    public void Website()
    {
        Application.OpenURL("http://www.psitireinflation.com/");
    }
    public void Features()
    {
        

        if (inSubMenu)
        {
            source.Stop();
            psi.SetInteger("State", 0);
            StartCoroutine(ResetIdle());
            
        }
        else
        {
            psi.SetInteger("State", 2);
        }

        inSubMenu = !inSubMenu;


    }
    public void Installation()
    {
        if (!inSubMenu)
        {
           
            psi.SetInteger("State", 3);
            StartCoroutine(PlaySlides());
        }
        else
        {
            window.SetInteger("State", 0);
            psi.SetInteger("State", 0);
            
            StartCoroutine(ResetIdle());
            
        }
        inSubMenu = !inSubMenu;
    }

    IEnumerator ResetIdle()
    {
        yield return new WaitForSeconds(introAnim.length / 2.0f);
        psi.SetInteger("State", 1);
    }

    IEnumerator PlaySlides()
    {
        yield return new WaitForSeconds(1.5f);
        source.Play();
        window.SetInteger("State", 1);
    }
}
